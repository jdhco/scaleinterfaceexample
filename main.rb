require 'sinatra'
require "sinatra/reloader"

set :port, 3000
set :bind, '0.0.0.0'

get '/' do
  if params[:postUrl]
      erb :scale, locals: {  description: params[:description],
                            weighflow_post_url: params[:postUrl],
                            return_url: params[:returnUrl] }
    else
      erb :home
  end
end

post '/' do 
  redirect '/'
end