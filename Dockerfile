FROM ruby:2-alpine

EXPOSE 3000

WORKDIR /app
ADD . /app

RUN bundle install

CMD ruby main.rb